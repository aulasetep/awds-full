#
# Cookbook Name:: apache
# Recipe:: default
#
#

package "build-essential" do
    action :install
end

package "git" do
  action :install
end

package "vim" do
  action :install
end

package "ruby-full" do
  action :install
end

# DNS
package "bind9" do
  action :install
end

package "bind9-doc" do
  action :install
end

package "dnsutils" do
  action :install
end

service "bind9" do
  action [:start]
end


# MySQL Server
package "mysql-server" do
  action :install
end

# Apache
package "apache2" do
  action :install
end

package "libapache2-mod-proxy-html" do
  action :install
end

package "libxml2-dev" do
  action :install
end

execute "a2enmod" do
  command "sudo a2enmod proxy proxy_ajp proxy_http rewrite deflate headers proxy_balancer proxy_connect proxy_html"
  action :run
end

service "apache2" do
  action :restart
end


# Nodejs
package "curl" do
  action :install
end

package "software-properties-common" do
  action :install
end

execute "curl" do
  command "curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -"
  action :run
end

package "nodejs" do
  action :install
end


# Local Domain configs
cookbook_file '/etc/bind/named.conf.options' do
  source 'named.conf.options'
  action :create
end

cookbook_file '/etc/bind/named.conf.local' do
  source 'named.conf.local'
  action :create
end

cookbook_file '/etc/bind/db.awds.local' do
  source 'db.awds.local'
  action :create
end

cookbook_file '/etc/bind/db.50.168.192' do
  source 'db.50.168.192'
  action :create
end

cookbook_file '/etc/resolv.conf' do
  source 'resolv.conf'
  owner 'root'
  action :create
end

service "bind9" do
  action [:restart]
end

# Front-end config
cookbook_file '/etc/apache2/sites-available/awds.local.conf' do
  source 'awds.local.conf'
  owner 'root'
  action :create
end

execute 'activate-site' do
  command 'sudo a2ensite awds.local.conf'
  action :run
end

execute 'npm-install-build-front' do
  command '(cd /vagrant/awds-files/automated_dns/ && sudo npm install && sudo npm run build)'
  action :run
end

execute 'deploy-front' do
  command '(sudo mkdir -p /var/www/awds.local/public_html && sudo cp -Rfv /vagrant/awds-files/automated_dns/dist/* /var/www/awds.local/public_html)'
  action :run
end

execute 'www-permission' do
  command 'sudo chmod -R 755 /var/www'
  action :run
end

service 'apache2' do
  action :restart
end


# Engine configuration
execute 'createDatabase' do
  command 'mysql -uroot < /vagrant/awds-files/awds-handlebards-engine/db.sql'
  action :run
end

execute 'install-npm-engine' do
  command 'npm --prefix /vagrant/awds-files/awds-handlebards-engine install /vagrant/awds-files/awds-handlebards-engine'
  action :run
end

execute 'install-npm-nodemon' do
  command 'npm --prefix /vagrant/awds-files/awds-handlebards-engine install -g nodemon /vagrant/awds-files/awds-handlebards-engine'
  action :run
end

cookbook_file '/etc/systemd/system/awds.service' do
  source 'awds.service'
  action :create
end

execute 'set-exec' do
  command 'sudo chmod +x /vagrant/awds-files/awds-handlebards-engine/run-server.sh'
  action :run
end

execute 'enable-service' do
  command 'sudo systemctl daemon-reload && sudo systemctl enable awds'
  action :run
end

service 'awds' do
  action :start
end

execute 'set-exec2' do
  command 'sudo chmod +x /vagrant/awds-files/awds-handlebards-engine/cpFiles.sh'
  action :run
end

cron 'run_cpFiles' do
  action :create
  minute '*/3'
  user 'root'
  command '/vagrant/awds-files/awds-handlebards-engine/cpFiles.sh &>/dev/null'
end