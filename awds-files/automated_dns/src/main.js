import Vue from 'vue'
import App from './App.vue'
import router from './router'
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import BootstrapVue from "bootstrap-vue";
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueGoodTable from 'vue-good-table';
import 'vue-good-table/dist/vue-good-table.css'
import Toasted from 'vue-toasted';
import VueLocalStorage from 'vue-localstorage'

Vue.use(VueGoodTable);
library.add(faCoffee)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(BootstrapVue);
Vue.use(VueLocalStorage);

var toasterOptions = {
  theme: "toasted-primary",
  position: "bottom-right",
  duration: 5000,
  className: 'm-2'
};
Vue.use(Toasted, toasterOptions);

Vue.config.productionTip = false

new Vue({
  router,
  render: function (h) { return h(App) }
}).$mount('#app')

