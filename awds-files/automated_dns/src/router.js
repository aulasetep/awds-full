import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/login/Login.vue'
import DNS from './components/dns/DNS.vue'
import Users from './components/users/Users.vue'
import Instances from './components/instances/Instances.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    // {
    //   path: '/dns/about',
    //   name: 'about',
    //   component: function () {
    //     return import('./views/About.vue')
    //   }
    // },
    {
      path: '/dns', component: DNS,
      children: [
        {
          path: 'users',
          component: Users
        },
        {
          path: 'instances',
          component: Instances
        }
      ]
    },
    { path: "*", redirect: '/login' }
  ]
});

router.beforeEach((to, from, next) => {
  var current = Vue.localStorage.get("app-user", null);
  if (current || to.name == "login")
    next();
  else next({ name: 'login' });
});

export default router;