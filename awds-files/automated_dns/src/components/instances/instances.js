import { VueGoodTable } from 'vue-good-table';
import axios from 'axios';
import config from '../../vue.config'

export default {
    data() {
        return {
            table: 'instances-table',
            searchTerm: '',
            buttons: {
                create: true,
                update: false,
                delete: false
            },
            columns: [
                {
                    label: 'Descrição',
                    field: 'description',
                    html: false,
                    filterable: true,
                },
                {
                    label: 'Domínio',
                    field: 'domain',
                    html: false,
                    filterable: true,
                },
                {
                    label: 'IP',
                    field: 'ip',
                    html: false,
                    filterable: true,
                },
                {
                    label: 'Data de criação',
                    field: 'createdAt',
                    html: false,
                    filterable: false,
                },
                {
                    label: 'Usuário Associado',
                    field: 'user',
                    html: false,
                    filterable: true,
                },
            ],
            rows: [],
            options: {
                selectOptions: {
                    enabled: true,
                    selectOnCheckboxOnly: true, // only select when checkbox is clicked instead of the row
                    selectionInfoClass: 'custom-class',
                    selectionText: 'linhas selecionadas',
                    clearSelectionText: 'Limpar',
                },
                paginationOptions: {
                    enabled: true,
                    mode: 'records',
                    perPage: 10,
                    position: 'bottom',
                    dropdownAllowAll: false,
                    setCurrentPage: 1,
                    nextLabel: 'Próximo',
                    prevLabel: 'Anterior',
                    rowsPerPageLabel: 'Linhas por página',
                    ofLabel: 'de',
                    pageLabel: 'page', // for 'pages' mode
                    allLabel: 'All',
                }
            },
            showModal: false,
            action: 'Cadastrar',
            New: {
                id: null,
                description: null,
                ip: null,
                createdAt: null,
                user: null,
                userId: null
            }
        };
    },
    created() {
        this._SelectAll();
    },
    methods: {
        _SelectAll() {
            axios
                .get(config.API_URL + '/instances/')
                .then(response => {
                    this.rows = response.data;
                }).catch(error => {
                    this._OnError();
                });
        },
        ShowUpdate() {
            this.action = "Editar";
            //GET ID
            var current = this.$refs[this.table].selectedRows;
            if (current) {
                current = current[0];
                axios
                    .get(config.API_URL + '/instances/' + current.id)
                    .then(response => {
                        if (response.data) {
                            this.New = response.data[0];
                            this.showModal = true;
                        } else
                            this._OnError();
                    }).catch(error => {
                        this._OnError();
                    });
            }
        },
        ShowCreate() {
            this.action = "Cadastrar";
            this.showModal = true;
            //GET ID
            this.New = {
                id: null,
                description: null,
                ip: null,
                createdAt: null,
                user: null,
                userId: null
            }
        },
        Save() {
            if (this.New.id)
                this._Update();
            else
                this._Create();
        },
        SelectionChanged(params) {
            this.buttons.create = false;
            this.buttons.update = false;
            this.buttons.delete = false;

            if (params.selectedRows.length == 0)
                this.buttons.create = true;
            if (params.selectedRows.length == 1)
                this.buttons.update = true;
            if (params.selectedRows.length >= 1)
                this.buttons.delete = true;
        },
        Delete() {
            //GET ID
            var current = this.$refs[this.table].selectedRows;
            if (current) {
                current.forEach(element => {
                    axios
                        .get(config.API_URL + '/instances/delete/' + element.id)
                        .then(response => {
                            this.$toasted.success('Registro(s) excluído(s) com sucesso!');
                            this._SelectAll();
                        }).catch(error => {
                            this._OnError();
                        });
                });
            }
        },
        _Update() {
            this.showModal = false;
            var current = this.$localStorage.get("app-user", null);
            if (current) {
                this.New.userId = JSON.parse(current).id;
                var parsed = {
                    description: this.New.description,
                    ip: this.New.ip,
                    domain: this.New.domain,
                    userId: this.New.userId,
                    id: this.New.id
                };
                axios({
                    method: 'post',
                    url: config.API_URL + '/instances/edit/',
                    data: {
                        New: parsed
                    }
                }).then(response => {
                    this.$toasted.success('Registro atualizado com sucesso!');
                    this._SelectAll();
                }).catch(error => {
                    this._OnError();
                });
            }
        },
        _Create() {
            this.showModal = false;
            var current = this.$localStorage.get("app-user", null);
            if (current) {
                this.New.userId = JSON.parse(current).id;
                var parsed = {
                    description: this.New.description,
                    ip: this.New.ip,
                    domain: this.New.domain,
                    userId: this.New.userId
                };
                axios({
                    method: 'post',
                    url: config.API_URL + '/instances/create',
                    data: {
                        New: parsed
                    }
                }).then(response => {
                    this.$toasted.success('Registro criado com sucesso!');
                    this._SelectAll();
                }).catch(error => {
                    this._OnError();
                });
            }
        },
        _OnError() {
            this.$toasted.error('Falha na execução!');
        }
    },
    components: {
        VueGoodTable,
    },
    computed: {
        Disable() {
            // return false;
            return !(this.New.description && this.New.ip && this.New.domain);
        }
    }
};