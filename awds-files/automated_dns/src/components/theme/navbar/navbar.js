export default {
    name: "ui-navbar",
    data: function () {
        return {
            test: "Teste"
        }
    } ,
    props: {
      fixed: {
        type: String,
        default: '',
        validator(val) {
          return ['top', 'left', 'bottom', 'right'].includes(val)
        }
      }
    },
    mounted() {
      let classNames = []
  
      if (this.fixed !== '') {
        classNames.push(this.fixed, 'fixed')
      }
  
      this.$refs.menu.classList.add(...classNames)
    }
};