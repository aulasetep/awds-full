export default {
  name: "ui-sidebar",
  data: function () {
    return {
      test: "Teste"
    }
  },
  props: {
    open: {
      type: Boolean,
      require: true
    },
    animation: {
      type: String,
      default: 'uncover'
    },
    dimmed: {
      type: Boolean,
      default: false
    },
    closable: {
      type: Boolean,
      default: false
    },
    direction: {
      type: String,
      default: 'left'
    },
    width: {
      type: String,
      default: ''
    }
  },
  mounted() {
    let classNames = []

    classNames.push(this.direction)

    if (this.animation !== '') {
      classNames.push(...this.animation.split(' '))
    }

    if (this.width !== '') {
      classNames.push(...this.width.split(' '))
    }

    if (this.visible) {
      classNames.push('visible')
    }

    this.$refs.sidebar.classList.add(...classNames)
  },
  data() {
    return {
      duration: 500,
      visible: this.open,
      stopAnimatingTimer: 0
    }
  },
  watch: {
    open: function (val) {
      this.visible = !this.visible
      if (this.visible) {
        this.show()
      } else {
        this.hide()
      }
    }
  },
  methods: {
    show() {
      const sidebar = this.$refs.sidebar
      const pusher = this.$refs.pusher

      sidebar.classList.add('visible')
      if (this.dimmed) {
        pusher.classList.add('dimmed')
      }

      this.startAnimating(this.duration)
    },
    close() {
      if (this.closable && this.visible) {
        this.visible = false
        this.hide()
      }
    },
    hide() {
      const sidebar = this.$refs.sidebar
      const pusher = this.$refs.pusher

      sidebar.classList.remove('visible')

      if (this.dimmed) {
        pusher.classList.remove('dimmed')
      }

      this.startAnimating(this.duration)
    },
    startAnimating(duration) {
      const sidebar = this.$refs.sidebar

      clearTimeout(this.stopAnimatingTimer)

      sidebar.classList.add('animating')

      this.stopAnimatingTimer = setTimeout(() => {
        sidebar.classList.remove('animating')
      }, duration)
    }
  }
};