import axios from 'axios';
import config from '../../vue.config'

export default {
    data: function () {
        return {
            Login: {
                username: null,
                password: null
            }
        }
    },
    created() {
        this.$localStorage.remove('app-user');
    },
    methods: {
        Authenticate(login, password) {
            this._DatabaseVerifier(login.password);
        },
        _DatabaseVerifier() {
            axios({
                method: 'post',
                url: config.API_URL + '/auth/login/',
                data: {
                    New: this.Login
                }
            }).then(response => {
                var current = response.data;
                if (current) {
                    this.$localStorage.set("app-user", JSON.stringify(current[0]));
                    this.$toasted.success("Autenticação efetuada com sucesso!");
                    this.$router.push({ path: "dns/instances" });
                } else this.$toasted.error("Falha na autenticação!");
            }).catch(error => {
                this._OnError();
            });
        },
        _OnError() {
            this.$toasted.error('Falha na autenticação!');
        }
    },
    computed: {
        Disable: function () {
            // return false;
            return !(this.Login.username && this.Login.password);
        }
    }
};