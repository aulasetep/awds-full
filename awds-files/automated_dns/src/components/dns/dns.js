import Sidebar from "@/components/theme/sidebar/Sidebar.vue";
import Navbar from "@/components/theme/navbar/Navbar.vue";
import axios from 'axios';
import config from '../../vue.config'

export default {
  name: "dns",
  components: {
    'ui-sidebar': Sidebar,
    'ui-navbar': Navbar,
  },
  data() {
    return {
      sidebarVisible: false,
      Password: {
        old: null,
        new: null,
        id: null
      },
      showModalPassword: false,
      Username: null,
    }
  },
  created() {
    var current = this.$localStorage.get("app-user", null);
    if (current)
      this.Username = JSON.parse(current).name_complete;
  },
  computed: {
    visible() {
      return this.sidebarVisible
    },
    DisableModalPassword() {
      return !(this.Password.old && this.Password.new);
    }
  },
  methods: {
    onAttach() {
      this.sidebarVisible = !this.sidebarVisible
    },
    Save() {
      this.showModalPassword = false;
      var current = this.$localStorage.get("app-user", null);
      if (current) {
        this.Password.id = JSON.parse(current).id;
        axios({
          method: 'post',
          url: config.API_URL + '/auth/resetpassword/',
          data: {
            Password: this.Password
          }
        }).then(response => {
          this.$toasted.success('Senha atualizada com sucesso!');
        }).catch(error => {
          this.$toasted.error('Falha na atualização de senha!');
        });
      }
    },
    Logout() {
      this.$localStorage.remove('app-user');
      this.$router.push({ path: "login" });
    },
    ShowCreate() {
      this.Password = {
        old: null,
        new: null,
        id: null
      };
      this.showModalPassword = true;
    }
  }
};