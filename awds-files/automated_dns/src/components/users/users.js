import { VueGoodTable } from 'vue-good-table';
import axios from 'axios';
import config from '../../vue.config'

export default {
    data() {
        return {
            table: 'users-table',
            searchTerm: '',
            buttons: {
                create: true,
                update: false,
                delete: false
            },
            columns: [
                {
                    label: 'Nome',
                    field: 'name_complete',
                    html: false,
                    filterable: true,
                },
                {
                    label: 'Username',
                    field: 'username',
                    html: false,
                    filterable: true,
                },
                {
                    label: 'Data de criação',
                    field: 'createdAt',
                    html: false,
                    filterable: false,
                },
            ],
            rows: [],
            options: {
                selectOptions: {
                    enabled: true,
                    selectOnCheckboxOnly: true, // only select when checkbox is clicked instead of the row
                    selectionInfoClass: 'custom-class',
                    selectionText: 'linhas selecionadas',
                    clearSelectionText: 'Limpar',
                },
                paginationOptions: {
                    enabled: true,
                    mode: 'records',
                    perPage: 10,
                    position: 'bottom',
                    dropdownAllowAll: false,
                    setCurrentPage: 1,
                    nextLabel: 'Próximo',
                    prevLabel: 'Anterior',
                    rowsPerPageLabel: 'Linhas por página',
                    ofLabel: 'de',
                    pageLabel: 'page', // for 'pages' mode
                    allLabel: 'All',
                }
            },
            showModal: false,
            action: 'Cadastrar',
            New: {
                id: null,
                name_complete: null,
                username: null,
                password: null,
                createdAt: null
            }
        };
    },
    created() {
        this._SelectAll();
    },
    methods: {
        _SelectAll() {
            axios
                .get(config.API_URL + '/auth/')
                .then(response => {
                    this.rows = response.data;
                }).catch(error => {
                    this._OnError();
                });
        },
        ShowUpdate() {
            this.action = "Editar";
            //GET ID
            var current = this.$refs[this.table].selectedRows;
            if (current) {
                current = current[0];
                axios
                    .get(config.API_URL + '/auth/' + current.id)
                    .then(response => {
                        if (response.data) {
                            this.New = response.data[0];
                            this.showModal = true;
                        } else
                            this._OnError();
                    }).catch(error => {
                        this._OnError();
                    });
            }
        },
        ShowCreate() {
            this.action = "Cadastrar";
            this.showModal = true;
            //GET ID
            this.New = {
                id: null,
                name_complete: null,
                username: null,
                password: null
            }
        },
        Save() {
            if (this.New.id)
                this._Update();
            else
                this._Create();
        },
        SelectionChanged(params) {

            this.buttons.create = false;
            this.buttons.update = false;
            this.buttons.delete = false;

            if (params.selectedRows.length == 0)
                this.buttons.create = true;
            if (params.selectedRows.length == 1)
                this.buttons.update = true;
            if (params.selectedRows.length >= 1)
                this.buttons.delete = true;

        },
        Delete() {
            //GET ID
            var current = this.$refs[this.table].selectedRows;
            if (current) {
                var currentUser = this.$localStorage.get("app-user", null);
                if (currentUser) {
                    var userId = JSON.parse(currentUser).id;
                    current.forEach(element => {
                        if (userId == element.id) {
                            this.$toasted.error('Não é permitido deletar o usuário atual!');
                        } else {
                            axios
                                .get(config.API_URL + '/auth/delete/' + element.id)
                                .then(response => {
                                    this.$toasted.success('Registro(s) excluído(s) com sucesso!');
                                    this._SelectAll();
                                }).catch(error => {
                                    this._OnError();
                                });
                        }
                    });
                }
            }
        },
        _Update() {
            this.showModal = false;
            var parsed = {
                id: this.New.id,
                name_complete: this.New.name_complete,
                username: this.New.username
            };
            axios({
                method: 'post',
                url: config.API_URL + '/auth/edit/',
                data: {
                    New: parsed
                }
            }).then(response => {
                this.$toasted.success('Registro atualizado com sucesso!');
                this._SelectAll();
            }).catch(error => {
                this._OnError();
            });
        },
        _Create() {
            this.showModal = false;
            var parsed = {
                name_complete: this.New.name_complete,
                username: this.New.username,
                password: this.New.password
            };
            axios({
                method: 'post',
                url: config.API_URL + '/auth/create',
                data: {
                    New: parsed
                }
            }).then(response => {
                this.$toasted.success('Registro criado com sucesso!');
                this._SelectAll();
            }).catch(error => {
                this._OnError();
            });
        },
        _OnError() {
            this.$toasted.error('Falha na execução!');
        }
    },
    components: {
        VueGoodTable,
    },
    computed: {
        Disable() {
            // return false;
            return !(this.New.name_complete && this.New.username &&
                ((this.New.password && !this.New.id) || (this.New.id)));
        }
    }
};

