const mysql      = require('mysql');

function connMySQL() {
    return mysql.createConnection({
        host     : 'localhost',
        user     : 'root',
        password : '',
        database : 'awds'
    });
}

module.exports = function() {
    return connMySQL;
};