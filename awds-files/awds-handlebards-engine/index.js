const express = require("express");
const bodyParser = require("body-parser");

const users = require("./routes/users")
const instances = require("./routes/instances")

const app = express();

app.config = () => {
    
    var allowCrossDomain = function (req, res, next) {
        res.header('Access-Control-Allow-Origin', "*");
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-Type');
        next();
    }

    app.use(allowCrossDomain);
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
}

app.routes = () => {
    app.use("/auth", users);
    app.use("/instances", instances);
}

app.config();
app.routes();

module.exports = app;