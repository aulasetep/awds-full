const Handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

const getTemplate = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('template.hbs', 'utf8', (err, content) => {
            if (!!err) reject(err)
            resolve(content)
        })
    })
}

const checkIfFileExists = (domain) => {
    return new Promise((resolve, reject) => {

        const filename = `configs/${domain}.conf`;

        fs.access(filename, fs.F_OK, (err) => {
            if (!err) {
                fs.unlink(filename, (err) => {
                    if (err) reject(err);
                    resolve(0);
                });
            } else {
                resolve(1)
            }
        });
    })
}

const createConfig = (file, domain, ip) => {
    return new Promise((resolve, reject) => {
        const template = Handlebars.compile(file)
        const result = template({
            domain: domain,
            ip: ip
        })

        fs.writeFile(`configs/${domain}.conf`, result, err => {
            if (!!err) reject(err);
            resolve()
        });
    })
}

const generate = (domain, ip) => {
    return new Promise((resolve, reject) => {
        checkIfFileExists(domain);

        getTemplate()
            .then(file => createConfig(file, domain, ip))
            .then(() => {
                resolve({ filename: `${domain}.conf` });
            })
            .catch(err => {
                console.log('An error was found!')
                reject(err)
            })
    });
}

//Function to refresh the configs
const refreshConfigs = () => {
    return new Promise((resolve, reject) => {
        resetAllFiles()
            .then(resolve())
            .catch(err => {
                console.log('Falha na execução!');
                reject(err);
            })
    });
}

//Function to delete all files inside configs
const resetAllFiles = () => {
    return new Promise((resolve, reject) => {
        var directory = `configs`;
        fs.readdir(directory, (err, files) => {
            if (err) throw err;
            for (const file of files) {
                if (!!!(file === (".gitkeep"))){
                    fs.unlink(path.join(directory, file), err => {
                        if (err) throw err;
                    });
                }
            }
        });
    });
}

module.exports = { generate: generate, refreshConfigs: refreshConfigs }