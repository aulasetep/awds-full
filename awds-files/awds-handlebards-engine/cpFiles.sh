#! /bin/bash


#echo "Copy DNS configs - files db.* "
sudo cp /vagrant/awds-files/awds-handlebards-engine/configs/db.* /etc/bind/ 
#echo "Restarting bind9.service"
sudo systemctl restart bind9.service

#echo "Copy apache configs - files sites.conf "
sudo cp /vagrant/awds-files/awds-handlebards-engine/configs/*.conf /etc/apache2/sites-available/ 

#echo "Enable sites"
for f in *.local.conf
    do sudo a2ensite $f;
done

#echo "Restarting apache2.service"
sudo systemctl restart apache2.service


