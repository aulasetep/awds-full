#! /usr/bin/env node

const db = require("./dbConnection")();
const mysql = require('mysql')
const Handlebars = require('handlebars');
const fs = require('fs');
const path = require('path');

const getInstances = () => {
    return new Promise((resolve, reject) => {
        let query = `SELECT * FROM instances`
        db().query(query, function (err, results, fields) {
            if (!!err) reject(err)
            else resolve(results)            
        });
    })
}

const fillDnsConfigs = () => {
      
    return new Promise((resolve, reject ) => {
       
        getInstances().then((instances) => {
                    
            instances.forEach((item) => {            
                
                const domain = item.domain.split('.')
                const lineDbDomain = `\n${domain[0]}    CNAME   dns     ;${item.id}`
                const fileDbDomain = getDbDomainFileName(item.domain)
                fs.appendFile(`configs/${fileDbDomain}`, lineDbDomain, function (err, file) {
                    if (err) reject(err);                
                    else resolve()
                });
                
                const lineDbIP = `\n2               PTR   ${item.domain}.     ; instance Id = ${item.id}` // 2 -> it is the ip from dns server
                const fileDbIP = getDbIPFileName(item.ip)
                fs.appendFile(`configs/${fileDbIP}`, lineDbIP, function (err, file) {
                    if (err) reject(err);                
                    else resolve()                
                });
            })
        })
    })
}


const getTemplate = (dbDnsFile) => {
    return new Promise((resolve, reject) => {

        fs.readFile(`template.${dbDnsFile}.hbs`, 'utf8', (err, content) => {
            if (!!err) reject(err)
            resolve(content)
        })
    })
}


const checkIfFileExists = (file) => {
    return new Promise((resolve, reject) => {

        const filename = `configs/${file}`;

        fs.access(filename, fs.F_OK, (err) => {
            if (!err) {
                fs.unlink(filename, (err) => {
                    if (err) reject(err);
                    resolve(0);
                });
            } else {
                resolve(1)
            }
        });
    })
}

const createConfig = (file, dbDnsFile) => {
    return new Promise((resolve, reject) => {
        const template = Handlebars.compile(file)
        const result = template({            
        })

        fs.writeFile(`configs/${dbDnsFile}`, result, err => {
            if (!!err) reject(err);
            resolve()
        });
    })
}

const getDbIPFileName = (ip) => {

    const temp = ip.split('.')
    const fileDbIP = "db." + temp[2]  + "." + temp[1] + "." + temp[0]

    return fileDbIP
}

const getDbDomainFileName = (domain) => {
    
    temp = domain.split('.')
    const fileDbDomain = "db." + temp[1]  + "." + temp[2]
        
    return fileDbDomain    
    
}

const generateDnsConfig = (domain, ip) => {
    return new Promise((resolve, reject) => {
        
        var domainFileName = getDbDomainFileName(domain);
        var ipFileName = getDbIPFileName(ip);

        checkIfFileExists(domainFileName);
        checkIfFileExists(ipFileName);

        getTemplate(domainFileName)
            .then(file => createConfig(file, domainFileName))
            .then(() => {
                resolve({ filename: `${domainFileName}` });
            })
            .catch(err => {
                console.log('An error was found!')
                reject(err)
            })
        
        getTemplate(ipFileName)
            .then(file => createConfig(file, ipFileName))
            .then(() => {
                resolve({ filename: `${ipFileName}` });
           })
            .catch(err => {
                console.log('An error was found!')
                reject(err)
            })
        
        fillDnsConfigs()
            .then(() => {
                console.log("FIM na teoria")
            })
            .catch(err => {
                console.log('An error was found!')
                reject(err)
            })

    });    
    
}

//Function to refresh the configs
const refreshConfigs = () => {
    return new Promise((resolve, reject) => {
        resetAllFiles()
            .then(resolve())
            .catch(err => {
                console.log('Falha na execução!');
                reject(err);
            })
    });
}

//Function to delete all files inside configs
const resetAllFiles = () => {
    return new Promise((resolve, reject) => {
        var directory = `configs`;
        fs.readdir(directory, (err, files) => {
            if (err) throw err;
            for (const file of files) {
                if (!!!(file === (".gitkeep"))){
                    fs.unlink(path.join(directory, file), err => {
                        if (err) throw err;
                    });
                }
            }
        });
    });
}


// const a = process.argv

// generateDnsConfig(a[2],a[3])



module.exports = { generateDnsConfig: generateDnsConfig}