DROP DATABASE IF EXISTS awds;
CREATE DATABASE awds;

use awds;

CREATE TABLE users(
   id int auto_increment not null,
   name_complete varchar(255) not null,
   username varchar(255) not null,
   password varchar(255) not null,
   createdAt timestamp DEFAULT CURRENT_TIMESTAMP not null,
   PRIMARY KEY(id)
);

insert into users(name_complete, username, password, createdAt) values ("Administrator", "admin", "admin", NOW());

CREATE TABLE instances(
    id int auto_increment not null,
    domain varchar(255) not null,
    description varchar(255) not null,
    ip varchar(255) not null,
    createdAt timestamp DEFAULT CURRENT_TIMESTAMP,
    userId int not null,
    PRIMARY KEY (id),
    FOREIGN KEY (userId) REFERENCES users(id)
);