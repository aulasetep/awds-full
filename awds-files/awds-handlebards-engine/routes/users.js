const express = require("express");
const db = require("../dbConnection")();
const routerUser = express.Router();
const table = "users";

routerUser.get("/", (req, res) => {
    var query = `SELECT id, name_complete, username, DATE_FORMAT(createdAt, '%d/%m/%Y %H:%i') createdAt FROM ${table}`;
    db().query(query, function (err, results, fields) {
        if (err) res.status(404).send("Falha na execução!");
        res.status(200).send(results);
    });
});

routerUser.get("/:id", (req, res) => {
    const object = req.params.id;
    var query = `SELECT id, name_complete, username FROM ${table} WHERE id= ${object}`;
    db().query(query, function (err, results, fields) {
        if (err) res.status(404).send("Falha na execução!");
        res.status(200).send(results);
    });
});

routerUser.post("/login", (req, res) => {
    const data = req.body.New;
    let query = `SELECT id, name_complete FROM ${table} WHERE (username = "${data.username}" AND password = "${data.password}")`;
    db().query(query, function (err, results, fields) {
        if(err) res.status(404).send("Falha na execução!");
        if (results.length === 1) {
            res.status(200).send(results);
        } else if(results.length === 0) {
            res.status(404).send("Not found")
        } else {
            res.status(500).send("Erro!")
        }
    });
});

routerUser.post("/create", (req, res) => {
    const data = req.body.New;
    let queryVerifier = `SELECT id FROM ${table} WHERE username= "${data.username}"`;
    db().query(queryVerifier, function (err, results, fields) {
        if (err || results.length === 1) {
            res.status(404).send("Ja existe!");
        } else {        
            let query = `INSERT INTO ${table}(name_complete, username, password, createdAt) VALUES ("${data.name_complete}", "${data.username}", "${data.password}", NOW())`;
            db().query(query, function (err, results1, fields) {
                if (err) res.status(404).send("Falha na execução!");
                res.status(200).send("adicionado!");
            });
        }
    });
});

routerUser.post("/edit", (req, res) => {
    const data = req.body.New;
    let queryVerifier = `SELECT id FROM ${table} WHERE username= "${data.username}" AND id != ${data.id}`;
    db().query(queryVerifier, function (err, results, fields) {
        if (err || results.length === 0) res.status(404).send(err);
        let query = `UPDATE ${table}
            SET name_complete = "${data.name_complete}", 
            username = "${data.username}",
            createdAt = NOW() 
            WHERE id = ${data.id}`;
        db().query(query, function (err, results, fields) {
            if (err) res.status(404).send("Falha na execução!");
            res.status(200).send(results);
        });
    });
});

routerUser.get("/delete/:id", (req, res) => {
    const id = req.params.id;
    db().query(`DELETE FROM ${table} WHERE id = ${id}`, function (err, results, fields) {
        if (err) res.status(404).send("Falha na execução!");
        res.status(200).send(results);
    });
});

routerUser.post("/resetpassword/", (req, res) => {
    const data = req.body.Password;
    let queryVerifier = `SELECT id FROM ${table} WHERE password = "${data.old}" AND id = ${data.id}`;
    db().query(queryVerifier, function (err, results, fields) {
        if (err || results.length == 0) res.status(404).send(err);
        db().query(`UPDATE ${table} SET password = "${data.new}" WHERE id = ${data.id}`, function (err, results, fields) {
            if (err) res.status(404).send("Falha na execução!");
            res.status(200).send(results);
        });
    });
});

module.exports = routerUser;

/*
curl -X POST localhost:3030/auth/login  -H "Content-Type: application/json" \
  -d '{"New":{"name": "Natália","username":"nath","password": "123"}}'
*/