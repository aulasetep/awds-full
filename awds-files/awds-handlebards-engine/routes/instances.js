const express = require("express");
const db = require("../dbConnection")();
const vhost = require("../vhost");
const dnsConfigs = require("../dnsConfigs")
const routerInstances = express.Router();
const table = "instances";

routerInstances.get("/", (req, res) => {
    let query = `SELECT T.id, T.domain, T.description, T.ip, DATE_FORMAT(T.createdAt, '%d/%m/%Y %H:%i') createdAt, T.userId, U.name_complete 'user' FROM ${table} T
    LEFT JOIN users U ON U.id = T.userId`;
    db().query(query, function (err, results, fields) {
        if (err) res.status(404).send("Falha na execução!");
        res.status(200).send(results);
    });
});

routerInstances.get("/:id", (req, res) => {
    const object = req.params.id;
    let query = `SELECT * FROM ${table} WHERE id= ${object}`;
    db().query(query, function (err, results, fields) {
        if (err) res.status(404).send("Falha na execução!");
        res.status(200).send(results);
    });
});

routerInstances.post("/create", (req, res) => {
    const data = req.body.New;

    let queryVerifier = `SELECT id FROM ${table} WHERE domain = "${data.domain}"`;
    db().query(queryVerifier, function (err, results, fields) {
        if (err || results.length > 0)
        res.status(404).send(err);
        let query = `INSERT INTO ${table} (domain, description, ip, userId) VALUES ("${data.domain}" , "${data.description}", "${data.ip}", ${data.userId})`;
        db().query(query,
            function (err, results, fields) {
                if (err) res.status(404).send("Falha na execução!");;
                if (results.affectedRows === 1) {

                    vhost.refreshConfigs()
                        .then(() => {
                            let filesQuery = `SELECT ip, domain FROM ${table}`;
                            db().query(filesQuery, function (err, results, fields) {
                                if (err) res.status(404).send(err);
                                results.forEach(function (item) {
                                    vhost.generate(item.domain, item.ip)
                                        .catch(error => { res.status(500).send(error) });                                    
                                });

                                dnsConfigs.generateDnsConfig(data.domain, data.ip)            
                                    .catch(error => {console.log(error)});

                                res.send();
                            });
                        })
                        .catch(error => { res.status(500).send(error) });
                } else
                    res.status(404).send("not-found")
            }

        );

        
    });
});



routerInstances.post("/edit", (req, res) => {
    const data = req.body.New;
    let queryVerifier = `SELECT id FROM ${table} WHERE domain = "${data.domain}" AND ip = "${data.ip}" AND id != ${data.id}`;
    db().query(queryVerifier, function (err, results, fields) {
        if (err || results.length > 0)
            res.status(404).send(err);
            let query = `UPDATE instances 
                SET ip = "${data.ip}", 
                userId = ${data.userId}, 
                description = "${data.description}", 
                domain = "${data.domain}", 
                createdAt = NOW() 
                WHERE id = ${data.id}`
                db().query(query, function (err, results, fields) {
                    if (err) res.status(404).send("Falha na execução!");
                    vhost.refreshConfigs()
                        .then(() => {
                            let filesQuery = `SELECT ip, domain FROM ${table}`;
                            db().query(filesQuery, function (err, results, fields) {
                                if (err) res.status(404).send(err);
                                results.forEach(function (item) {
                                    vhost.generate(item.domain, item.ip)
                                        .catch(error => { res.status(500).send(error) });
                                });
                                res.status(400).send(err);
                            });
                        })
                        .catch(error => { res.status(404).send(error) });
                });
    });
});

routerInstances.get("/delete/:id", (req, res) => {
    const id = req.params.id;
    let query = `DELETE FROM ${table} WHERE id = ${id}`;
    db().query(query, function (err, results, fields) {
        if (err) res.status(404).send("Falha na execução!");
        vhost.refreshConfigs()
            .then(() => {
                let filesQuery = `SELECT ip, domain FROM ${table}`;
                db().query(filesQuery, function (err, results, fields) {
                    if (err) res.status(404).send(err);
                    results.forEach(function (item) {
                        vhost.generate(item.domain, item.ip)
                            .catch(error => { res.status(200).send(error) });
                    });
                    res.status(404).send(err);
                });
            })
            .catch(error => { res.status(500).send(error) });
    });
})
module.exports = routerInstances;

/*
ip, domain, description, userId, createdAt

curl -X POST localhost:3030/instances/create  -H "Content-Type: application/json" \
  -d '{"ip": "10.0.0.0","domain":"nath.com","description": "123456"}'


curl -X POST localhost:8080/instances/create  -H "Content-Type: application/json"
 -d '{"ip": "10.0.0.0","domain":"nath.com","userId":1,"description": "123456"}'
*/